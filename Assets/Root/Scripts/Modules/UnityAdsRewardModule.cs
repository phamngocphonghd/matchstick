﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;

namespace Root
{
    public class UnityAdsRewardModule : IAdsRewardService
    {
        private Action CloseCallBack;
        const string c_AdsID = "rewardedVideo";

        public void Init()
        {

        }

        public void ShowAds(Action closeCallback = null)
        {
            if (closeCallback != null)
            {
                CloseCallBack = closeCallback;
            }

            ShowOptions options = new ShowOptions();
            options.resultCallback = HandleShowResult;

            Advertisement.Show(c_AdsID, options);
        }

        void HandleShowResult(ShowResult result)
        {
            if (result == ShowResult.Finished)
            {
                Debug.Log("Video completed - Offer a reward to the player");
                RunCallback();
            }
            else if (result == ShowResult.Skipped)
            {
                Debug.LogWarning("Video was skipped - Do NOT reward the player");
                RunCallback();
            }
            else if (result == ShowResult.Failed)
            {
                Debug.LogError("Video failed to show");
                RunCallback();
            }
        }

        void RunCallback()
        {
            if (CloseCallBack != null)
            {
                CloseCallBack();
            }
        }

        public bool IsAdsAvailable()
        {
            return Advertisement.IsReady(c_AdsID);
        }
    }
}

