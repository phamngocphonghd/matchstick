﻿using System;

namespace Root
{
    public interface IAnalyticABTestService
    {

        void SetUpTest(ABTestConfig[] configs, int versionCode, Func<bool> funcIsNewInstall);

        void SetLogAnalytic(Action<string> actionLog);

        bool IsActive(string keyNameAB);

        int GetValue(string keyNameAB);

        ABTestConfig GetConfig(string keyNameAB);
        
    }

    [Serializable]
    public class ABTestConfig
    {
        public string KeyName;
        public bool IsActive;
        public bool IsOnlyNewInstall;
        public bool IsOverride;
        public string[] ListEventsAB;
        public int[] ListRandomRatio;
        public int DefaultValue;
    }
}

