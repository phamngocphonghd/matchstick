﻿using System;
using System.Collections.Generic;

namespace Root
{
	public interface IInAppPurchaseService
	{
		
		void ConnectInit ();

		bool IsProductPurchased (string productID);

		void PurchaseProduct (string productID, Action<bool> callBack);

		void RestorePurchase (Dictionary<string, Action<bool>> callbacks);

	}

    [Serializable]
    public class IapConfig
    {
        public string androidKey;
        public IapProduct[] products;
    }

    [Serializable]
    public class IapProduct
    {
        public string Id;
        public IapType Type;
        public string AndroidId;
        public string IosId;
    }

    public enum IapType
    {
        Consume,
        NonConsume,
        Subscription
    }
}

