﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Root;
[CreateAssetMenu(fileName = "ServiceConfig", menuName = "Service/ServiceConfig")]
public class ServicesConfig : ScriptableObject
{
    [SerializeField]
    public int BundleVersion;
    [SerializeField]
    public ABTestConfig[] Configs;
}
