﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBehaviour
{
    /// <summary>
    /// replace MonoBehaviour Start()
    /// </summary>
    void Begin();

    /// <summary>
    /// replace MonoBehaviour Update()
    /// </summary>
    void FrameUpdate();

    /// <summary>
    /// replace MonoBehaviour FixedUpdate()
    /// </summary>
    void TimeUpdate();

    /// <summary>
    /// replace monobehaviour gameObject properties
    /// </summary>
    /// <returns></returns>
    GameObject GetGameObject();
}