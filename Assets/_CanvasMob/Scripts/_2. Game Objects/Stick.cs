﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Prime31.ZestKit;
public class Stick : MonoBehaviour {
    public int idStick;
    public int indexInList;
    public bool isShadow;
    public bool isStandby=false;
    public GameObject ShadowStick;
    public GameObject LightStick;
    public ObjPos startPos;
    public ObjPos endPos;
    public Spawner spawner;
    
    private void OnMouseDown()
    {
        if (isStandby) return;
        if(isShadow)
        {
            if (spawner.countStandbyLight() == 0) return;
            spawner.lastLightStandby().gameObject.transform.ZKlocalPositionTo(gameObject.transform.position, 0.3f)
             .start();
            spawner.lastLightStandby().gameObject.transform.ZKlocalRotationTo(gameObject.transform.rotation).start();
            spawner.lastLightStandby().isStandby = false;
            spawner.lastLightStandby().idStick = idStick;
            spawner.AddStickToStartLightList(spawner.lastLightStandby());
            spawner.RemoveLastStandbyStick();
            spawner.CheckWin();
        }
        else
        {
            
        }
    }

    public void SetStickShadow()
    {
        if(isShadow)
        {
            ShadowStick.SetActive(true);
            LightStick.SetActive(false);
        }
    }
}
