﻿using System.Collections.Generic;
using UnityEngine;

public enum GameType
{
    Remove,
    Add,
    Move
}

public class Spawner : MonoBehaviour, IBehaviour
{
    LevelData levelGame;

    public GameType gameType;
    [SerializeField]
    StandbyStick standby;
    List<Stick> m_listShadow;
    List<int> m_listLightStart;
    List<int> m_listLightEnd;
    public void Begin()
    {
        levelGame = new LevelData();
        m_listShadow = new List<Stick>();
        m_listLightStart = new List<int>();
        m_listLightEnd = new List<int>();
        standby.Begin();
    }

    public void FrameUpdate()
    {

    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public void TimeUpdate()
    {

    }

    public void SpawnTutorial()
    {

    }

    public void SpawnMap(int level)
    {
        levelGame = DataManager.instance.loadLevelChallenge(level);
        // Spawn Shadow
        for (int i = 0; i < levelGame.shadowStickData.Count; i++)
        {
            GameObject obj;
            obj = Instantiate(GameManager.instance.StickPrefab, new Vector3(((float)levelGame.shadowStickData[i].endPos.xPos + (float)levelGame.shadowStickData[i].startPos.xPos) / 2f, ((float)levelGame.shadowStickData[i].endPos.yPos + (float)levelGame.shadowStickData[i].startPos.yPos) / 2f) + (0.25f * levelGame.width + 0.25f) * Vector3.left + (0.25f * levelGame.height + 0.25f) * Vector3.up, Quaternion.identity);
            if (levelGame.shadowStickData[i].endPos.xPos == levelGame.shadowStickData[i].startPos.xPos)
            {
                if (levelGame.shadowStickData[i].endPos.yPos > levelGame.shadowStickData[i].startPos.yPos)
                {
                    obj.transform.rotation = Quaternion.Euler(0, 0, 90);
                }
                else
                {
                    obj.transform.rotation = Quaternion.Euler(0, 0, -90);
                }
            }
            else
            {
                if (levelGame.shadowStickData[i].endPos.xPos > levelGame.shadowStickData[i].startPos.xPos)
                {
                    obj.transform.rotation = Quaternion.Euler(0, 0, 0);
                }
                else
                {
                    obj.transform.rotation = Quaternion.Euler(0, 0, 180);
                }
            }
            var stick = obj.GetComponent<Stick>();
            stick.isShadow = true;
            stick.idStick = levelGame.shadowStickData[i].idStick;
            stick.SetStickShadow();
            stick.spawner = this;
            m_listShadow.Add(stick);
        }

        //Spawn Light Stick
        for (int i = 0; i < levelGame.startLightStickData.Count; i++)
        {
            GameObject obj;
            obj = Instantiate(GameManager.instance.StickPrefab, new Vector3(((float)levelGame.startLightStickData[i].endPos.xPos + (float)levelGame.startLightStickData[i].startPos.xPos) / 2f, ((float)levelGame.startLightStickData[i].endPos.yPos + (float)levelGame.startLightStickData[i].startPos.yPos) / 2f) + (0.25f * levelGame.width + 0.25f) * Vector3.left + (0.25f * levelGame.height + 0.25f) * Vector3.up, Quaternion.identity);
            if (levelGame.startLightStickData[i].endPos.xPos == levelGame.startLightStickData[i].startPos.xPos)
            {
                if (levelGame.startLightStickData[i].endPos.yPos > levelGame.startLightStickData[i].startPos.yPos)
                {
                    obj.transform.rotation = Quaternion.Euler(0, 0, 90);
                }
                else
                {
                    obj.transform.rotation = Quaternion.Euler(0, 0, -90);
                }
            }
            else
            {
                if (levelGame.startLightStickData[i].endPos.xPos > levelGame.startLightStickData[i].startPos.xPos)
                {
                    obj.transform.rotation = Quaternion.Euler(0, 0, 0);
                }
                else
                {
                    obj.transform.rotation = Quaternion.Euler(0, 0, 180);
                }
            }
            var stick = obj.GetComponent<Stick>();
            stick.isShadow = false;
            stick.SetStickShadow();
            stick.spawner = this;
            stick.idStick = levelGame.startLightStickData[i].idStick;
            AddStickToStartLightList(stick);
        }
        //Add to list end
        for (int i = 0; i < levelGame.endLightStickData.Count; i++)
        {
            m_listLightEnd.Add(levelGame.endLightStickData[i].idStick);
        }

        m_listLightEnd.Sort();
        //Change GameType
        gameType = levelGame.gameType;

        //Spawn Standby Stick
        SpawnStandbyStick();
    }

    public void RemoveStickFromLightList(int index)
    {
        m_listLightStart.RemoveAt(index);
    }

    public void AddStickToStartLightList(Stick stick)
    {
        m_listLightStart.Add(stick.idStick);
        stick.indexInList = m_listLightStart.Count - 1;
    }

    public void SpawnStandbyStick()
    {
        for (int i = 0; i < levelGame.countObjUse; i++)
        {
            GameObject obj;
            obj = Instantiate(GameManager.instance.StickPrefab, standby.gameObject.transform.position, Quaternion.identity);
            var stick = obj.GetComponent<Stick>();
            stick.isShadow = true;
            stick.SetStickShadow();
            stick.spawner = this;
            stick.isStandby = true;
            standby.AddShadowStickToList(stick);
            obj.transform.rotation = Quaternion.Euler(0, 0, 90);
        }

        if (gameType == GameType.Add)
        {
            for (int i = 0; i < levelGame.countObjUse; i++)
            {
                GameObject obj;
                obj = Instantiate(GameManager.instance.StickPrefab, standby.gameObject.transform.position, Quaternion.identity);
                var stick = obj.GetComponent<Stick>();
                stick.isShadow = false;
                stick.SetStickShadow();
                stick.spawner = this;
                stick.isStandby = true;
                standby.AddLightStickToList(stick);
                obj.transform.rotation = Quaternion.Euler(0, 0, 90);
            }
        }

        standby.OrderListShadow();
        standby.OrderListLight();
    }

    public bool CompareListToWin()
    {
        m_listLightStart.Sort();
        if (m_listLightStart.Count < m_listLightEnd.Count)
        {
            return false;
        }
        else
        {
            for (int i = 0; i < m_listLightStart.Count; i++)
            {
                if (m_listLightStart[i] != m_listLightEnd[i])
                {
                    return false;
                }
            }

            return true;
        }
    }

    public void CheckWin()
    {
        if (CompareListToWin())
        {
            UIManager.instance.ShowWinUI();
        }
    }

    public void RecycleInWinGame()
    {

    }

    public void ShowHint()
    {

    }

    public int countStandbyLight()
    {
        return standby.countOfListLight();
    }

    public Stick lastLightStandby()
    {
        return standby.lastLightStandby();
    }

    public void RemoveLastStandbyStick()
    {
        standby.RemoveLightStickFromList();
    }
}