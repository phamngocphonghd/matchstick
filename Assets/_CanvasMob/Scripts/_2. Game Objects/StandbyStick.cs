﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandbyStick : MonoBehaviour, IBehaviour
{
    List<Stick> m_listShadow;
    List<Stick> m_listLight;
    public void Begin()
    {
        m_listShadow = new List<Stick>();
        m_listLight = new List<Stick>();
    }

    public void FrameUpdate()
    {

    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public void TimeUpdate()
    {

    }

    public void AddLightStickToList(Stick stick)
    {
        m_listLight.Add(stick);
    }

    public void AddShadowStickToList(Stick stick)
    {
        m_listShadow.Add(stick);
    }

    public void RemoveLightStickFromList()
    {
        m_listLight.RemoveAt(m_listLight.Count - 1);
    }

    public void OrderListShadow()
    {
        for (int i = 0; i < m_listShadow.Count; i++)
        {
            m_listShadow[i].gameObject.transform.position = transform.position + 0.25f *i* Vector3.right;
        }
    }

    public void OrderListLight()
    {
        if (m_listLight.Count == 0) return;
        for (int i = 0; i < m_listLight.Count; i++)
        {
            m_listLight[i].gameObject.transform.position = transform.position + 0.25f *i* Vector3.right;
        }
    }

    public int countOfListLight()
    {
        return m_listLight.Count;
    }

    public Stick lastLightStandby()
    {
        return m_listLight[m_listLight.Count - 1];
    }
}
