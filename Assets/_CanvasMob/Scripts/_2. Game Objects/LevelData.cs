﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable]
public class LevelData
{
    public int width;
    public int height;
    public string gameTitle;
    public List<StickData> shadowStickData;
    public List<StickData> endLightStickData;
    public List<StickData> startLightStickData;
    public List<int> solutions;
    public GameType gameType;
    public int countObjUse;
    /*public LevelData()
    {
        gameTitle = "";
        shadowStickData = null;
        endLightStickData = null;
        startLightStickData = null;
        solutions = null;
    }*/
}

[Serializable]
public class StickData
{
    public int idStick;
    public ObjPos endPos;
    public ObjPos startPos;

    public StickData()
    {
        endPos = new ObjPos();
        startPos = new ObjPos();
    }
}