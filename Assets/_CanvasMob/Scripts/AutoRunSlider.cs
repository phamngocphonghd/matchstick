﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class AutoRunSlider : MonoBehaviour
{
    public GameObject m_HandImage;
    public GameObject m_Slider;
    RectTransform m_rt;
    RectTransform m_CircleInSlider;
    float m_SliderWidth;
    bool m_RunL2R;
    public float m_SpeedSlider;
    // Use this for initialization
    void Start()
    {
        m_rt = m_Slider.GetComponent<RectTransform>();
        m_SliderWidth = m_rt.rect.width;
        m_CircleInSlider = m_HandImage.GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_RunL2R)
        {
            float xPos = m_CircleInSlider.anchoredPosition.x + Time.deltaTime * m_SpeedSlider;
            m_CircleInSlider.anchoredPosition = new Vector3(xPos, 0, 0);

        }
        else
        {
            float xPos = m_CircleInSlider.anchoredPosition.x - Time.deltaTime * m_SpeedSlider;
            m_CircleInSlider.anchoredPosition = new Vector3(xPos, 0, 0);
        }
        if (m_CircleInSlider.anchoredPosition.x >= m_SliderWidth / 2)
        {
            m_RunL2R = false;
        }
        if (m_CircleInSlider.anchoredPosition.x <= -m_SliderWidth / 2)
        {
            m_RunL2R = true;
        }
    }
}
