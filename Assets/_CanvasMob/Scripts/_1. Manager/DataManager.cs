﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : Singleton<DataManager>
{
    public LevelData dataLevel;
    //public Level dataTut;
    public bool m_IsSoundOn;
    public int maxLevelUnlock;
    public bool isDidTut;

    private void Start()
    {

    }

    //public Level LoadDataTut()
    //{
    //    var ta = Resources.Load("tutorial");
    //    dataTut = JsonUtility.FromJson<Level>(ta.ToString());
    //    return dataTut;
    //}

    public LevelData loadLevelChallenge(int level)
    {
        var ta = Resources.Load(level.ToString());
        dataLevel = JsonUtility.FromJson<LevelData>(ta.ToString());
        return dataLevel;
    }

    public int GetMaxLevelUnlock()
    {
        return ServicesManager.DataSecure().GetInt("MaxLevelUnlock", 1);
    }

    public bool GetSoundOnOff()
    {
        return ServicesManager.DataSecure().GetBool("IsSoundOn", true);
    }

    public bool GetDidTut()
    {
        return ServicesManager.DataSecure().GetBool("IsDidTut", false);
    }

    public int GetMoney()
    {
        return ServicesManager.DataSecure().GetInt("Money", 0);
    }

    public void SaveData()
    {
        ServicesManager.DataSecure().SetInt("MaxLevelUnlock", maxLevelUnlock);
        ServicesManager.DataSecure().SetBool("IsSoundOn", m_IsSoundOn);
        ServicesManager.DataSecure().SetBool("IsDidTut", isDidTut);
    }

    public int GetMaxLevelUnlockInHard()
    {
        return ServicesManager.DataSecure().GetInt("MaxLevelUnlockInHard", 1);
    }
}
