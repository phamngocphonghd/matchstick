﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager> {

    [SerializeField]
    AudioSource m_AudioSource;

    [SerializeField]
    AudioClip m_SoundStartGame;
    [SerializeField]
    AudioClip m_SoundWin;
    [SerializeField]
    AudioClip m_SounOnClickButton;
    [SerializeField]
    AudioClip m_SoundDrawCell;
    [SerializeField]
    AudioClip m_SoundLastDrawCell;
    [SerializeField]
    AudioClip m_SoundBackGround;

    public void PlaySoundClickButton()
    {
        m_AudioSource.PlayOneShot(m_SounOnClickButton);
    }

    public void PlaySoundWinGame()
    {
        m_AudioSource.PlayOneShot(m_SoundWin);
    }

    public void PlaySoundStartGame()
    {
        m_AudioSource.PlayOneShot(m_SoundStartGame);
    }

    public void SoundOn()
    {
        m_AudioSource.volume = 1f;
    }

    public void SoundOff()
    {
        m_AudioSource.volume = 0f;
    }

    public void SetSound()
    {
        if (GameManager.Instance.isSoundOn)
        {
            m_AudioSource.volume = 1f;
        }
        else
        {
            m_AudioSource.volume = 0f;
        }
    }

    public void PlaySoundDraw()
    {
        m_AudioSource.PlayOneShot(m_SoundDrawCell);
    }

    public void PlaySoundBackGround()
    {
        m_AudioSource.PlayOneShot(m_SoundBackGround);
    }

    public void PlaySoundLastDraw()
    {
        m_AudioSource.PlayOneShot(m_SoundLastDrawCell);
    }
}
